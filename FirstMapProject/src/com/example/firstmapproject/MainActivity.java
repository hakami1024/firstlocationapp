package com.example.firstmapproject;

import java.util.ArrayList;
import java.util.Date;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends FragmentActivity implements OnConnectionFailedListener, ConnectionCallbacks,
															LocationListener, OnMapLongClickListener{

	private static final int UPDATE_INTERVAL = 60*1000;
	private static final int FASTEST_UPDATE_INTERVAL = 500;
	private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9111;
	private static final String TAG = "MainActivity";
	
	GoogleApiClient mLocationClient;
    
	LocationRequest mLocationRequest;
	TextView mLocationTV; 
	GoogleMap mMap;
	ArrayList<LatLng> mMarkerPos = new ArrayList<LatLng>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mLocationTV = (TextView)findViewById(R.id.location_tv);
		Button mMockButton = (Button)findViewById(R.id.mock_location_button);
		
		LocationManager prov = (LocationManager) getSystemService(LOCATION_SERVICE);
		for( String s: prov.getProviders(false))
		{
			Log.d(TAG, s);
		}
		
		servicesConnected();
		
		mLocationRequest = LocationRequest.create();
		mLocationRequest.setPriority(
                LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        
        mLocationClient = new GoogleApiClient.Builder(this, this, this)
        .addApi(LocationServices.API)
        .build();
        
        setUpMapIfNeeded();  
        /*
        mMockButton.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Random rand = new Random();
				int pos = rand.nextInt(mMarkerPos.size());
				Location location = new Location("flp");
				location.setLatitude( mMarkerPos.get(pos).latitude );
				location.setLongitude( mMarkerPos.get(pos).longitude );
				location.setAltitude(0);
				location.setAccuracy(3.0f);
				location.setTime((new Date()).getTime());
		
				//mMockLocationClient.setMockLocation( location );
				
				//LocationServices.FusedLocationApi.setMockLocation(mLocationClient, location);
				Location loc = LocationServices.FusedLocationApi.getLastLocation(mLocationClient);
	            				
				if(loc!=null)
					Log.d(TAG, +loc.getLatitude()+", "+loc.getLongitude());
				else Log.e(TAG, "null location");

			}
		});*/
        //mLocationClient = new LocationClient(this, this, this);        
	}
	
	private void setUpMapIfNeeded() {
		
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                mMap.setOnMapLongClickListener(this);
            }
        }
    }
    
	private boolean servicesConnected() {

		int resultCode =
                GooglePlayServicesUtil.
                        isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == resultCode) {
            Log.d("Location Updates", "Google Play services is available.");
            return true;
        } else 
            return false;
    }

	
	@Override
	protected void onStart() {
	 	super.onStart();
	 	mLocationClient.connect();
    }
	
	@Override
    public void onLocationChanged(Location location) {
		Log.d(TAG, "onLocationChanged");
		Date updateDate = new Date(location.getTime());
        mLocationTV.setText( ""+location.getLatitude()+", "+location.getLongitude()+"  (updated on "+updateDate+")");
        LocationServices.FusedLocationApi.getLastLocation(mLocationClient);
	}
 
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		Log.d(TAG, "onConnectionFailed");
		if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult( this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Connection failed", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "Connection failed with error code: "+connectionResult.getErrorCode());
        }
		
	} 

	@Override
	public void onConnected(Bundle arg0) {
		Log.d(TAG, "onConnected");
		 LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, 
	        	    mLocationRequest, this);
	}

	
	@Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        // Decide what to do based on the original request code
		Log.d(TAG, "in onActivityResult");
        switch (requestCode) {
            case CONNECTION_FAILURE_RESOLUTION_REQUEST :
            /*
             * If the result code is Activity.RESULT_OK, try
             * to connect again
             */
                switch (resultCode) {
                    case Activity.RESULT_OK :
                    Log.d(TAG, "something bad with connection");
                    break;
                    default:
                    	Log.d(TAG, "bad connected");
                }
                break;
           default:
        	   Log.d(TAG, "Connection error with code "+requestCode);
        }
    } 

	@Override
	public void onConnectionSuspended(int arg0) {
		Log.d(TAG, "onConnectionSustpended");
	}

	@Override
	public void onMapLongClick(LatLng pos) {
		if( mMap != null )
		{
			final MarkerOptions markerOptions = new MarkerOptions().position(pos);
			
			AlertDialog.Builder alertBuild = new AlertDialog.Builder(this);
						
		    LayoutInflater inflater = getLayoutInflater();
			View dialogLayout = inflater.inflate(R.layout.marker_dialog, null);

		    final EditText nameET = (EditText) dialogLayout.findViewById(R.id.name_edit_text);
		    final EditText radiusET = (EditText) dialogLayout.findViewById(R.id.radius_edit_text);
		    final LatLng position = pos;
		    
		    alertBuild.setView(dialogLayout);

		    alertBuild.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int whichButton) {
		            Editable name = nameET.getText();
		            markerOptions.title(name.toString());
		            Editable radius = radiusET.getText();
		            
		            CircleOptions circleOptions = new CircleOptions();
		            circleOptions.center(position);
		            circleOptions.radius( Integer.parseInt(radius.toString()) );
		            circleOptions.fillColor(0x20ff0000);
		            circleOptions.strokeColor(Color.RED);
		            Log.d(TAG, ""+circleOptions.getStrokeWidth());
		            circleOptions.strokeWidth(1.0f);
		            
		            mMap.addMarker(markerOptions);
		            mMap.addCircle(circleOptions);
		            mMarkerPos.add(position);
		            
		            Intent proximityIntent = new Intent("com.example.firstmapproject.proximity");
		            proximityIntent.putExtra("lat", position.latitude);
		            proximityIntent.putExtra("lng", position.longitude);
		            proximityIntent.putExtra("name", name.toString());
		            PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, proximityIntent,Intent.FLAG_ACTIVITY_NEW_TASK);
		            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		            locationManager.addProximityAlert(position.latitude, position.longitude, 20, -1, pendingIntent);
		        }
		    });

		    alertBuild.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int whichButton) {}
		    });
		    
		    alertBuild.show();

		}
	}

}
