package com.example.firstmapproject;

import android.app.Activity;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class ProximityActivity extends Activity {
	

	private static final String TAG = "ProximityActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "in ProximitActivity's onCreate");
		
		 boolean proximity_entering = getIntent().getBooleanExtra(LocationManager.KEY_PROXIMITY_ENTERING, true);
		 
	        double lat = getIntent().getDoubleExtra("lat", 0);
	 
	        double lng = getIntent().getDoubleExtra("lng", 0);
	        String name = getIntent().getStringExtra("name");
	 
	        if(proximity_entering){
	            Toast.makeText(getBaseContext(),"Entering the region"  ,Toast.LENGTH_LONG).show();
	        }else{
	            Toast.makeText(getBaseContext(),"Exiting the region"  ,Toast.LENGTH_LONG).show();
	        }
            finish();

	}
}
